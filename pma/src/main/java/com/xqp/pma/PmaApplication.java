package com.xqp.pma;

import com.xqp.pma.dao.ProjectRepository;
import com.xqp.pma.dao.StudentRepository;
import com.xqp.pma.entities.Project;
import com.xqp.pma.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class PmaApplication {



	public static void main(String[] args) {
		SpringApplication.run(PmaApplication.class, args);
	}


	}



