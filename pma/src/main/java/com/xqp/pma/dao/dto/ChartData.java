package com.xqp.pma.dao.dto;

public interface ChartData {
    String getLabel();
    long getValue();
}
