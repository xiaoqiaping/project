package com.xqp.pma.dao.dto;

public interface StudentProject {
    //需定义方法名称：get+属性名
    String getName();
    String getWechatId();
    int getProjectCount();

}
