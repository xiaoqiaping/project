package com.xqp.pma.dao;

import com.xqp.pma.dao.dto.ChartData;
import com.xqp.pma.entities.Project;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


//数据实体的类名和数据实体中的ID字段的类型。
public interface ProjectRepository extends CrudRepository<Project,Long> {
    @Override
    public List<Project> findAll();

    @Query(nativeQuery = true,value = "SELECT stage AS label, COUNT(*) AS value FROM project GROUP BY stage")
    List<ChartData> getProjectStatus();
}
