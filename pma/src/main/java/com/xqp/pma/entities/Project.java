package com.xqp.pma.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "project_seq")
    @SequenceGenerator(name="project_seq",sequenceName = "project_seq",allocationSize = 1)
    private long projectId;
    private String name;
    private String stage;//项目状态
    private String description;//项目描述
    @ManyToMany(
        cascade={
                CascadeType.DETACH,
                CascadeType.MERGE,
                CascadeType.REFRESH,
                CascadeType.PERSIST},
        fetch=FetchType.LAZY
        )
    @JoinTable(
        name="project_student",
        joinColumns=@JoinColumn(name="project_id"),
        inverseJoinColumns=@JoinColumn(name="student_id")
    )

    private List<Student> students;

    public Project() {
    }

    //由于这个项目是有数据库支持的项目，所以Project构造器参数不需要包含projectId，此字段应为数据库负责生成。
    public Project( String name, String stage, String description) {
        this.name = name;
        this.stage = stage;
        this.description = description;
    }



    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public String getStage() {
        return stage;
    }

    public String getDescription() {
        return description;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }


    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", name='" + name + '\'' +
                ", stage='" + stage + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public void addStudent(Student student) {
        if(this.students==null){
            this.students=new ArrayList<>();

        }
        this.students.add(student);
    }
}
