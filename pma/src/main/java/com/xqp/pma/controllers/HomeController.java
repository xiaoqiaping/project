package com.xqp.pma.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xqp.pma.dao.ProjectRepository;
import com.xqp.pma.dao.StudentRepository;
import com.xqp.pma.dao.dto.ChartData;
import com.xqp.pma.dao.dto.StudentProject;
import com.xqp.pma.entities.Project;
import com.xqp.pma.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {
    @Value("${version}")
    private String version;

    @Autowired
    ProjectRepository projectRepository;
    @Autowired
    StudentRepository studentRepository;
    @GetMapping("/")
    public String displayHome(Model model) throws JsonProcessingException {

        model.addAttribute("versionNumber",version);

        List<Project> projects = projectRepository.findAll();
        model.addAttribute("projects",projects);

//        List<Student> students=studentRepository.findAll();
//        model.addAttribute("students",students);

        List<ChartData> projectStatusData=projectRepository.getProjectStatus();
        //转化projectStatusData为JSON结构，方便再JS脚本使用
        ObjectMapper objectMapper=new ObjectMapper();
        String jsonString=objectMapper.writeValueAsString(projectStatusData);
        //[{"未开始，1"}，{"进行中，2"}，{"已完成，1"}，]
        model.addAttribute("projectStatusData",jsonString);



        List<StudentProject> studentProjects=studentRepository.studentProjects();
        model.addAttribute("studentProjects",studentProjects);

        return "main/home";

    }
}
